local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local helpers = require("helpers")
local pad = helpers.pad

local screen_sel_timeout = 1
local traybox_timeout = 1

local screen_sel = require("noodle.text_taglist")
local screen_sel_box = wibox(
  {
    visible = false,
    ontop = true,
    shape = helpers.rrect(beautiful.border_radius),
    type = "dock"
  })
screen_sel_box.width = dpi(300)
screen_sel_box.height = dpi(50)
screen_sel_box.x = awful.screen.focused().geometry.width / 2 - screen_sel_box.width / 2
screen_sel_box.y = awful.screen.focused().geometry.height / 2 - screen_sel_box.height / 2
screen_sel_box.bg = beautiful.sidebar_bg or beautiful.wibar_bg or "#111111"
screen_sel_box:setup {
  nil,
  {
    nil,
    screen_sel,
    nil,
    layout = wibox.layout.align.vertical,
    expand = "none"
  },
  nil,
  layout = wibox.layout.align.horizontal,
  expand = "none"
}

local sel_timer = nil
function show_screen_select()
  if sel_timer then
    sel_timer:stop()
  else
    screen_sel_box.visible = true
  end

  sel_timer = gears.timer.start_new (screen_sel_timeout, function ()
    screen_sel_box.visible = false
    sel_timer = nil
    return false
  end)
end



local activator_timer = nil
local pressure_timer = nil
local hide_timer = nil
awful.screen.connect_for_each_screen(function(s)
  -- Create a system tray widget
  s.systray = wibox.widget.systray()

  -- Create a wibox that will only show the tray
  -- Hidden by default. Can be toggled with a keybind.
  s.traybox = wibox({visible = false, ontop = true, shape = helpers.rrect(beautiful.border_radius), type = "dock"})
  s.traybox.width = dpi(120)
  s.traybox.height = beautiful.wibar_height - beautiful.screen_margin
  s.traybox.x = s.geometry.width - beautiful.screen_margin * 2 - s.traybox.width
  s.traybox.y = beautiful.screen_margin * 2
  s.traybox.bg = beautiful.bg_systray
  s.traybox:setup {
    pad(1),
    s.systray,
    pad(1),
    layout = wibox.layout.align.horizontal
  }
  s.traybox:buttons(gears.table.join(
                      -- Middle click - Hide traybox
                      awful.button({ }, 2, function ()
                          s.traybox.visible = false
                      end)
  ))
  s.traybox:connect_signal("mouse::enter", function ()
	if activator_timer then
      activator_timer:stop()
      activator_timer = nil
	end
	if hide_timer then
      hide_timer:stop()
      hide_timer = nil
	end
  end)
  -- Hide traybox when mouse leaves
  s.traybox:connect_signal("mouse::leave", function ()
	if hide_timer then
      hide_timer:stop()
	end
	if s.traybox.visible then
      hide_timer = gears.timer.start_new(0.20, function()
		s.traybox.visible = false
		hide_timer = nil
		return false
      end)
    end
  end)

  s:connect_signal("tag::history::update", function ()
    if not start_screen.visible then
      show_screen_select()
    end
  end)
end)

-- Show traybox when the mouse touches the rightmost edge of the wibar
traybox_activator = wibox({ x = mouse.screen.geometry.width - 1, y = 0, height = 1, width = 1, opacity = 0, visible = true, bg = beautiful.wibar_bg })
traybox_activator:connect_signal("mouse::enter", function ()
  if pressure_timer then
	pressure_timer:stop()
	pressure_timer = nil
  end
  pressure_timer = gears.timer.start_new(0.20, function ()
    mouse.screen.traybox.visible = true
	pressure_timer = nil
	return false
  end)

  if activator_timer then
    activator_timer:stop()
    activator_timer = nil
  end
end)
traybox_activator:connect_signal("mouse::leave", function ()
  if pressure_timer then
	pressure_timer:stop()
	pressure_timer = nil
  end
  if activator_timer then
    activator_timer:stop()
  end
  activator_timer = gears.timer.start_new(traybox_timeout, function ()
    mouse.screen.traybox.visible = false
    activator_timer = nil
    return false
  end)
end)


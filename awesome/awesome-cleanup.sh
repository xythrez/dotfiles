#!/bin/bash

function cleanup {
  pkill -u $(whoami) -xf "$1" 2> /dev/null
}

# Mpd widget
cleanup "mpc idleloop player"
cleanup "playerctl metadata --follow --format .+"
# Volume widget
cleanup "pactl subscribe"
# Ibus Daemon
cleanup "ibus-daemon"


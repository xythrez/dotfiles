#!/bin/bash
function run_once {
  if [ $(pgrep -u $(whoami) -cx "$1") -eq 0 ]; then
    $@&
    disown
  fi
}

run_once picom -b --config ~/.config/compton.conf
run_once mpd
run_once greenclip daemon
run_once ibus-daemon -d
run_once protonmail-bridge --no-window

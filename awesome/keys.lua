local awful = require("awful")
local naughty = require("naughty")
local gears = require("gears")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local helpers = require("helpers")

local keys = {}

-- Mod keys
superkey = "Mod4"
altkey = "Mod1"
ctrlkey = "Control"
shiftkey = "Shift"

-- {{{ Mouse bindings on desktop
keys.desktopbuttons = gears.table.join(
    awful.button({ }, 1, function ()
        mymainmenu:hide()
        if myclientmenu then
          myclientmenu:hide()
          myclientmenu = nil
        end
        sidebar.visible = false
        naughty.destroy_all_notifications()
    end),
    awful.button({ }, 3,
        function ()
          mymainmenu:toggle()
          if myclientmenu then
            myclientmenu:hide()
            myclientmenu = nil
          end
        end),

    -- Middle button - Toggle start screen
    awful.button({ }, 2, function ()
      mymainmenu:hide()
      if myclientmenu then
        myclientmenu:hide()
        myclientmenu = nil
      end
      start_screen_show()
    end),

    -- Scrolling - Switch tags
    awful.button({ }, 4, awful.tag.viewprev),
    awful.button({ }, 5, awful.tag.viewnext)
)
-- }}}

-- {{{ Key bindings
keys.globalkeys = gears.table.join(
    -- [UI Manipulation]
    -- Toggle sidebar
    awful.key({ superkey }, "grave", function() sidebar.visible = not sidebar.visible end,
        {description = "show or hide sidebar", group = "awesome"}),
    -- Start screen
    awful.key({ superkey }, "F1",
        function()
          start_screen_show()
        end,
        {description = "show start screen", group = "awesome"}),
    -- Main menu
    awful.key({ altkey }, "space",
        function ()
          mymainmenu:show()
          if myclientmenu then
            myclientmenu:hide()
            myclientmenu = nil
          end
        end,
        {description = "show main menu", group = "awesome"}),
    -- Toggle tray visibility
    awful.key({ superkey }, "F2",
        function ()
          awful.screen.focused().traybox.visible = not awful.screen.focused().traybox.visible
        end,
        {description = "toggle tray visibility", group = "awesome"}),
    -- Logout, Shutdown, Restart, Suspend, Lock
    awful.key({ ctrlkey, altkey }, "Delete",
        function ()
          exit_screen_show()
        end,
        {description = "exit", group = "awesome"}),

    -- Show Client List
    awful.key( { superkey }, "space",
        function()
          show_client_list()
        end,
        {description = "show client list", group = "awesome"}),

    -- Dismiss notifications
    awful.key( { superkey }, "Return", function()
      naughty.destroy_all_notifications()
    end,
        {description = "dismiss notification", group = "notifications"}),

    awful.key({ ctrlkey, altkey, shiftkey }, "F12", awesome.restart,
        {description = "reload awesome", group = "awesome"}),

    -- [Media Keys]

    awful.key({ }, "Menu",
        function ()
          awful.spawn.with_shell("xdotool click 3")
        end),
    -- Brightness
    awful.key( { }, "XF86MonBrightnessDown",
        function()
          -- awful.spawn.with_shell("xbacklight -dec 7")
          awful.spawn.easy_async_with_shell("xbacklight -dec 5", function()
            awesome.emit_signal("brightness_changed")
          end)
        end,
        {description = "decrease brightness", group = "brightness"}),
    awful.key( { }, "XF86MonBrightnessUp",
        function()
          -- awful.spawn.with_shell("xbacklight -inc 7")
          awful.spawn.easy_async_with_shell("xbacklight -inc 5", function()
            awesome.emit_signal("brightness_changed")
          end)
        end,
        {description = "increase brightness", group = "brightness"}),
    -- Volume Control
    awful.key( { }, "XF86AudioLowerVolume",
        function()
          awful.spawn.with_shell(volume_script .. " down")
        end,
        {description = "lower volume", group = "volume"}),
    awful.key( { }, "XF86AudioRaiseVolume",
        function()
          awful.spawn.with_shell(volume_script .. " up")
        end,
        {description = "raise volume", group = "volume"}),
    awful.key( { }, "XF86AudioMute",
        function()
          awful.spawn.with_shell(volume_script .. " toggle")
        end,
        {description = "(un)mute volume", group = "volume"}),
    awful.key( { }, "XF86AudioMicMute",
        function()
          awful.spawn.with_shell("amixer set Capture toggle")
        end,
        {description = "(un)mute mic", group = "volume"}),
    -- Media
    awful.key({ }, "XF86AudioNext", function() awful.spawn.with_shell("(playerctl next 2>/dev/null && mpc pause) || mpc next") end,
        {description = "next song", group = "media"}),
    awful.key({ }, "XF86AudioPrev", function() awful.spawn.with_shell("(playerctl previous 2>/dev/null && mpc pause) || mpc prev") end,
        {description = "previous song", group = "media"}),
    awful.key({ }, "XF86AudioPlay", function() awful.spawn.with_shell("(playerctl play-pause 2>/dev/null && mpc pause) || mpc toggle") end,
        {description = "toggle pause/play", group = "media"}),
    awful.key({ }, "XF86AudioStop", function() awful.spawn.with_shell("(playerctl stop 2>/dev/null && mpc pause)|| mpc stop") end,
        {description = "toggle pause/play", group = "media"}),
    -- Other
    awful.key({ }, "XF86Bluetooth", function() awful.spawn("blueman-manager") end,
        {description = "start bluetooth manager", group = "other"}),
    awful.key({ }, "XF86Search",
        function()
          sidebar.visible = false
          awful.spawn.with_shell("rofi -show drun")
        end,
        {description = "rofi drun launcher", group = "launcher"}),
    awful.key({ }, "XF86WLAN",
        function()
          awful.spawn.with_shell([[
          bash -c '
          if [ "$(nmcli radio wifi)" == "enabled" ]; then
              nmcli radio wifi off
          else
              nmcli radio wifi on
          fi']])
        end,
        {description = "toggle wifi", group = "other"}),
    awful.key({ }, "XF86Tools",
        function()
          awful.spawn("system-config-printer")
        end,
        {description = "show printer manager", group = "other"}),
    awful.key({ }, "XF86Display",
        function()
          awful.spawn("lxrandr")
        end,
        {description = "show display manager", group = "other"}),
    awful.key({ superkey }, "p",
        function()
          awful.spawn.with_shell("autorandr --change")
        end,
        {description = "auto configure display", group = "other"}),
    -- Screenshots
    awful.key( { ctrlkey }, "Print", function() awful.spawn.with_shell(screenshot_script) end,
        {description = "take full screenshot", group = "screenshots"}),
    awful.key( { superkey }, "Print", function() awful.spawn.with_shell(screenshot_script .. " -s") end,
        {description = "select area to capture", group = "screenshots"}),
    awful.key( { superkey, ctrlkey }, "Print", function() awful.spawn.with_shell(screenshot_script .. " -c") end,
        {description = "select area to copy to clipboard", group = "screenshots"}),

    -- [Client(s) Control]

    -- Kill all visible clients for the current tag
    awful.key({ shiftkey, altkey }, "F4",
        function ()
          local clients = awful.screen.focused().clients
          for _, c in pairs(clients) do
            c:kill()
          end
        end,
        {description = "kill all visible clients for the current tag", group = "gaps"}
    ),

    awful.key({ altkey }, "Tab",
        function ()
          awful.client.focus.byidx(1)
        end,
        {description = "focus next by index", group = "client"}),

    awful.key({ ctrlkey, altkey }, "Tab",
        function ()
          awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}),

    -- Toggle floating spawn
    awful.key({ superkey }, "w",
        function()
          float_new_win = not float_new_win
          local mode_icon, mode_name
          if float_new_win then
            mode_icon = beautiful.layout_floating
            mode_name = "floating"
          else
            mode_icon = beautiful.layout_tile
            mode_name = "tiling"
          end
          local notification = naughty.notify({
            title = "Window Placement",
            text = "Changed to " .. mode_name .. " mode",
            icon = mode_icon,
            timeout = 2,
            replaces_id = last_tiling_notification_id
          })
          last_tiling_notification_id = notification.id
        end,
        {description = "toggle floating for new windows", group = "client"}),

    -- [Quick Launch]
    -- Run program
    awful.key({ superkey }, "Tab",
        function()
          sidebar.visible = false
          awful.spawn.with_shell("rofi -show window")
        end,
        {description = "rofi window picker", group = "launcher"}),
    awful.key({ superkey }, "r",
        function()
          sidebar.visible = false
          awful.spawn.with_shell("rofi -show drun")
        end,
        {description = "rofi drun launcher", group = "launcher"}),
    awful.key({ superkey, ctrlkey}, "r",
        function()
          sidebar.visible = false
          awful.spawn.with_shell("rofi -show run")
        end,
        {description = "rofi run launcher", group = "launcher"}),
    -- Spawn file manager
    awful.key({ ctrlkey, altkey}, "f",
        function()
          sidebar.visible = false
          awful.spawn(filemanager)
        end,
        {description = "file manager", group = "launcher"}),
    -- Spawn terminal
    awful.key({ ctrlkey, altkey}, "t",
        function()
          sidebar.visible = false
          awful.spawn(terminal)
		end,
        {description = "terminal", group = "launcher"}),
    -- Spawn browser
    awful.key({ ctrlkey, altkey}, "b",
        function()
          sidebar.visible = false
          awful.spawn(browser)
        end,
        {description = "browser", group = "launcher"}),
    -- Raise mail
    awful.key({ ctrlkey, altkey}, "m",
        function ()
          sidebar.visible = false
          local matcher = function (c)
            return awful.rules.match(c, {class = 'Thunderbird'})
          end
          awful.spawn.raise_or_spawn("thunderbird", {}, matcher)
        end,
        {description = "mail", group = "launcher"}),
    -- Spawn clipboard history
    awful.key({ ctrlkey, altkey}, "c",
        function()
          sidebar.visible = false
          awful.spawn.with_shell("rofi -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}'")
        end,
    {description = "clipboard history", group = "launcher"}),


    -- [Tag Control]
    awful.key({ ctrlkey, altkey}, "h", awful.tag.viewprev,
        {description = "go to previous tag", group = "tag"}),
    awful.key({ ctrlkey, altkey}, "Left", awful.tag.viewprev,
        {description = "go to previous tag", group = "tag"}),
    awful.key({ ctrlkey, altkey}, "l", awful.tag.viewnext,
        {description = "go to next tag", group = "tag"}),
    awful.key({ ctrlkey, altkey}, "Right", awful.tag.viewnext,
        {description = "go to next tag", group = "tag"}),
    awful.key({ ctrlkey, altkey, shiftkey}, "h",
        function()
          local c = client.focus
          awful.tag.viewprev()
          if c then
            c:move_to_tag(awful.screen.focused().selected_tag)
          end
        end,
        {description = "move client to previous tag", group = "tag"}),
    awful.key({ ctrlkey, altkey, shiftkey}, "Left",
        function()
          local c = client.focus
          awful.tag.viewprev()
          if c then
            c:move_to_tag(awful.screen.focused().selected_tag)
          end
        end,
        {description = "move client to previous tag", group = "tag"}),
    awful.key({ ctrlkey, altkey, shiftkey}, "l",
        function()
          local c = client.focus
          awful.tag.viewnext()
          if c then
            c:move_to_tag(awful.screen.focused().selected_tag)
          end
        end,
        {description = "move client to next tag", group = "tag"}),
    awful.key({ ctrlkey, altkey, shiftkey}, "Right",
        function()
          local c = client.focus
          awful.tag.viewnext()
          if c then
            c:move_to_tag(awful.screen.focused().selected_tag)
          end
        end,
        {description = "move client to next tag", group = "tag"}),
    awful.key({ ctrlkey, shiftkey, superkey }, "h",
        function ()
          awful.tag.incncol(-1, nil, true)
        end,
        {description = "decrease the number of columns", group = "layout"}),
    awful.key({ ctrlkey, shiftkey, superkey }, "l",
        function ()
          awful.tag.incncol(1, nil, true)
        end,
        {description = "increase the number of columns", group = "layout"}),
    awful.key({ ctrlkey, shiftkey, superkey }, "k",
        function ()
          awful.tag.incnmaster(-1, nil, true)
        end,
        {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ ctrlkey, shiftkey, superkey }, "j",
        function ()
          awful.tag.incnmaster( 1, nil, true)
        end,
        {description = "increase the number of master clients", group = "layout"}),
    awful.key({ ctrlkey , superkey }, "h",
        function ()
          awful.tag.incmwfact(-0.05)
        end,
        {description = "decrease the master width factor", group = "layout"}),
    awful.key({  ctrlkey, superkey }, "l",
        function ()
          awful.tag.incmwfact(0.05)
        end,
        {description = "increase the master width factor", group = "layout"}),
    awful.key({ ctrlkey, superkey }, "k",
        function ()
          awful.client.incwfact(-0.05)
        end,
        {description = "decrease the window factor of the focused clients", group = "layout"}),
    awful.key({ ctrlkey, superkey }, "j",
        function ()
          awful.client.incwfact(0.05)
        end,
        {description = "increase the window factor of the focused clients", group = "layout"})
)

-- Client Bindings
keys.clientkeys = gears.table.join(
    awful.key({ altkey }, "F4",
        function (c)
          c:kill()
        end,
        {description = "kill the current focused client", group = "gaps"}
    ),
-- Toggle fullscreen
    awful.key({ superkey }, "f",
        function (c)
          c.fullscreen = not c.fullscreen
          c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ superkey }, "m",
        function (c)
          if c.floating then
            c.maximized = not c.maximized
            c:raise()
          end
        end,
        {description = "(un)maximize", group = "client"}),
    awful.key({ superkey }, "n",
        function (c)
          if c.floating then
            c.ontop = not c.ontop
          end
        end,
        {description = "toggle keep on top", group = "client"}),
    awful.key({ superkey }, "b",
        function (c)
          awful.titlebar.toggle(c)
        end,
        {description = "toggle titlebar", group = "client"}),
    awful.key({ superkey }, "z",
        function (c)
          if not c.floating then
            local curr = awful.client.next(-1)
            while curr.floating and c ~= curr do
              curr = awful.client.next(-1, curr)
            end
            if c == curr then
              c:raise()
            elseif curr then
              -- Fake it till you make it
              curr:emit_signal("request::activate", "client.focus.byidx",
                  {raise=true})
            end
          end
        end,
        {description = "focus previous tiled by index", group = "client"}),

    awful.key({ superkey }, "x",
        function (c)
          if not c.floating then
            local curr = awful.client.next(1)
            while curr.floating and c ~= curr do
              curr = awful.client.next(1, curr)
            end
            if c == curr then
              c:raise()
            elseif curr then
              -- Fake it till you make it
              curr:emit_signal("request::activate", "client.focus.byidx",
                  {raise=true})
            end
          end
        end,
        {description = "focus next tiled by index", group = "client"}),

    awful.key({ shiftkey, superkey }, "z",
        function (c)
          if not c.floating then
            local curr = awful.client.next(-1)
            while curr.floating and c ~= curr do
              curr = awful.client.next(-1, curr)
            end
            if c == curr then
              c:raise()
            elseif curr then
              c:swap(curr)
            end
          end
        end,
        {description = "focus previous tiled by index", group = "client"}),

    awful.key({ shiftkey, superkey }, "x",
        function (c)
          if not c.floating then
            local curr = awful.client.next(1)
            while curr.floating and c ~= curr do
              curr = awful.client.next(1, curr)
            end
            if c == curr then
              c:raise()
            elseif curr then
              -- Fake it till you make it
              c:swap(curr)
            end
          end
        end,
        {description = "focus next tiled by index", group = "client"}),

-- Toggle floating
    awful.key({ superkey, ctrlkey }, "f",
        function(c)
          if not c.always_floating then
            c.floating = not c.floating
          end
        end,
        {description = "toggle floating", group = "client"}),

-- Direction based focus
    awful.key({ superkey }, "Down",
        function(c)
          awful.client.focus.bydirection("down", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus down", group = "client"}),
    awful.key({ superkey }, "Up",
        function(c)
          awful.client.focus.bydirection("up", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus up", group = "client"}),
    awful.key({ superkey }, "Left",
        function(c)
          awful.client.focus.bydirection("left", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus left", group = "client"}),
    awful.key({ superkey }, "Right",
        function(c)
          awful.client.focus.bydirection("right", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus right", group = "client"}),
    awful.key({ superkey }, "j",
        function(c)
          awful.client.focus.bydirection("down", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus down", group = "client"}),
    awful.key({ superkey }, "k",
        function(c)
          awful.client.focus.bydirection("up", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus up", group = "client"}),
    awful.key({ superkey }, "h",
        function(c)
          awful.client.focus.bydirection("left", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus left", group = "client"}),
    awful.key({ superkey }, "l",
        function(c)
          awful.client.focus.bydirection("right", c)
          if client.focus then client.focus:raise() end
        end,
        {description = "focus right", group = "client"}),
-- Direction based swap
    awful.key({ shiftkey, superkey }, "Down",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("down", c)
          else
            helpers.move_to_edge(c, "down")
          end
        end,
        {description = "swap down", group = "client"}),
    awful.key({ shiftkey, superkey }, "Up",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("up", c)
          else
            helpers.move_to_edge(c, "up")
          end
        end,
        {description = "swap up", group = "client"}),
    awful.key({ shiftkey, superkey }, "Left",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("left", c)
          else
            helpers.move_to_edge(c, "left")
          end
        end,
        {description = "swap left", group = "client"}),
    awful.key({ shiftkey, superkey }, "Right",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("right", c)
          else
            helpers.move_to_edge(c, "right")
          end
        end,
        {description = "swap right", group = "client"}),
    awful.key({ shiftkey, superkey }, "j",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("down", c)
          else
            helpers.move_to_edge(c, "down")
          end
        end,
        {description = "swap down", group = "client"}),
    awful.key({ shiftkey, superkey }, "k",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("up", c)
          else
            helpers.move_to_edge(c, "up")
          end
        end,
        {description = "swap up", group = "client"}),
    awful.key({ shiftkey, superkey }, "h",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("left", c)
          else
            helpers.move_to_edge(c, "left")
          end
        end,
        {description = "swap left", group = "client"}),
    awful.key({ shiftkey, superkey }, "l",
        function(c)
          if not c.floating then
            awful.client.swap.bydirection("right", c)
          else
            helpers.move_to_edge(c, "right")
          end
        end,
        {description = "swap right", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
local ntags = 10
for i = 1, ntags do
    keys.globalkeys = gears.table.join(keys.globalkeys,
        -- View tag only.
        awful.key({ superkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        local current_tag = screen.selected_tag
                        -- Tag back and forth:
                        -- If you try to focus the same tag you are at,
                        -- go back to the previous tag.
                        -- Useful for quick switching after for example
                        -- checking an incoming chat message at tag 2
                        -- and coming back to your work at tag 1
                        if tag then
                           if tag == current_tag then
                               awful.tag.history.restore()
                           else
                               tag:view_only()
                           end
                        end
                        -- Simple tag view
                        --if tag then
                           --tag:view_only()
                        --end
                  end,
                  {description = "view tag #"..i, group = "tag"}),

        -- Toggle tag display.
        --awful.key({ superkey, ctrlkey }, "#" .. i + 9,
        --          function ()
        --              local screen = awful.screen.focused()
        --              local tag = screen.tags[i]
        --              if tag then
        --                 awful.tag.viewtoggle(tag)
        --              end
        --          end,
        --          {description = "toggle tag #" .. i, group = "tag"}),

        -- Move client to tag.
        awful.key({ superkey, shiftkey }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag and tag ~= screen.selected_tag then
                              client.focus:move_to_tag(tag)
                              tag:view_only()
                          end
                     end
                  end,
        {description = "move focused client to tag #"..i, group = "tag"})
        -- Move all visible clients to tag and focus that tag
        --awful.key({ superkey, altkey }, "#" .. i + 9,
        --          function ()
        --            local tag = client.focus.screen.tags[i]
        --            local clients = awful.screen.focused().clients
        --            if tag then
        --                for _, c in pairs(clients) do
        --                   c:move_to_tag(tag)
        --                end
        --                tag:view_only()
        --            end
        --          end,
        --{description = "move all visible clients to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        --awful.key({ superkey, ctrlkey, shiftkey }, "#" .. i + 9,
        --          function ()
        --              if client.focus then
        --                  local tag = client.focus.screen.tags[i]
        --                  if tag then
        --                      client.focus:toggle_tag(tag)
        --                  end
        --              end
        --          end,
        --          {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

-- Mouse buttons on the client (whole window, not just titlebar)
keys.clientbuttons = gears.table.join(
    awful.button({ }, 1,
        function (c)
          mymainmenu:hide();
          if myclientmenu then
            myclientmenu:hide()
            myclientmenu = nil
          end
          client.focus = c;
          c:raise()
        end),
    awful.button({ }, 3,
        function (c)
          mymainmenu:hide()
          if myclientmenu then
            myclientmenu:hide()
            myclientmenu = nil
          end
        end),
    awful.button({ altkey }, 1, awful.mouse.client.move),
    awful.button({ altkey }, 2, function (c) c:kill() end),
    awful.button({ altkey }, 3, function(c)
        awful.mouse.client.resize(c)
    end)
)
-- }}}

-- Set keys
root.keys(keys.globalkeys)
root.buttons(keys.desktopbuttons)

return keys

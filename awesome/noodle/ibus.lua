local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")

local helpers = require("helpers")

-- Configuration
local update_interval = 60
local default_color = beautiful.xcolor10

local ibus_toggle_script = [[
  bash -c '
  IM_DEFAULT="xkb:us::eng"
  IMS=( $IM_DEFAULT "libpinyin" )
  CURRENT_IM=$(ibus engine)
  NIM=0

  for IM in "${IMS[@]}"; do
	  ((NIM++))
	  echo $IM
	    if [ "$CURRENT_IM" == "$IM" ]; then
		    break
	    fi
    done

  if [[ $NIM == ${#IMS[@]} ]].."]]"..[[; then
    NIM=0
  fi

  ibus engine ${IMS[$NIM]}
  ']]

local ibus_display_script = [[
  bash -c '
  IM=$(ibus engine)
  if [[ $IM =~ eng$ ]].."]]"..[[; then
	  echo "EN"
	  exit 0
  elif [[ $IM =~ pinyin$ ]].."]]"..[[; then
	  echo "中"
	  exit 0
  fi
  echo "N/A"
  exit -1
  ']]

local ibus = wibox.widget{
  font = "sans bold 24",
  align  = 'center',
  valign = 'center',
  markup = helpers.colorize_text("N/A", default_color),
  widget = wibox.widget.textbox
}

local function update_widget(display)
  ibus.markup = helpers.colorize_text(display, default_color)
end

ibus:buttons(gears.table.join(
    awful.button({ }, 1, function ()
      awful.spawn.easy_async_with_shell(ibus_toggle_script, function ()
        awful.spawn.easy_async_with_shell(ibus_display_script, function (stdout)
          update_widget(stdout)
		end)
      end)
    end),
    awful.button({ }, 3, function ()
      awful.spawn.easy_async_with_shell(ibus_toggle_script, function ()
        awful.spawn.easy_async_with_shell(ibus_display_script, function (stdout)
          update_widget(stdout)
		end)
      end)
    end)
))

awful.widget.watch(ibus_display_script, update_interval, function(widget, stdout)
  update_widget(stdout)
end)

local start_delay = gears.timer.start_new (2, function ()
  awful.spawn.easy_async_with_shell(ibus_display_script, function (stdout)
    update_widget(stdout)
  end)
  return false
end)

return ibus

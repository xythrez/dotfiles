local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local helpers = require("helpers")
local pad = helpers.pad

-- Some commonly used variables
local playerctl_button_size = dpi(48)
local icon_size = dpi(36)
local progress_bar_width = dpi(215)

-- local progress_bar_margins = dpi(9)

-- Item configuration
local exit_icon = wibox.widget.imagebox(beautiful.poweroff_icon)
exit_icon.resize = true
exit_icon.forced_width = icon_size
exit_icon.forced_height = icon_size
local exit_text = wibox.widget.textbox("Exit")
exit_text.font = "sans 14"

local exit = wibox.widget{
  exit_icon,
  exit_text,
  layout = wibox.layout.fixed.horizontal
}
helpers.add_clickable_effect(exit)
exit:buttons(gears.table.join(
                 awful.button({ }, 1, function ()
                     helpers.reset_cursor()
                     exit_screen_show()
                     sidebar.visible = false
                 end)
))

local weather_widget = require("noodle.weather")
local weather_widget_icon = weather_widget:get_all_children()[1]
weather_widget_icon.forced_width = icon_size
weather_widget_icon.forced_height = icon_size
local weather_widget_pad = weather_widget:get_all_children()[2]
weather_widget_pad.font = "sans 14"
local weather_widget_text = weather_widget:get_all_children()[3]
weather_widget_text.font = "sans 14"

-- Dummy weather_widget for testing
-- (avoid making requests with every awesome restart)
-- local weather_widget = wibox.widget.textbox("[i] bla bla bla!")

local weather = wibox.widget{
    nil,
    weather_widget,
    nil,
    layout = wibox.layout.align.horizontal,
    expand = "none"
}

local network_widget = require("noodle.network")
local network_widget_icon = network_widget:get_all_children()[1]
network_widget_icon.forced_width = icon_size
network_widget_icon.forced_height = icon_size
helpers.add_clickable_effect(network_widget_icon)
local network_widget_pad = network_widget:get_all_children()[2]
network_widget_pad.font = "sans 14"
local network_widget_text = network_widget:get_all_children()[3]
network_widget_text.font = "sans 14"
network_widget_text.forced_height = icon_size
helpers.add_clickable_effect(network_widget_text)

local network = wibox.widget{
    nil,
    network_widget,
    nil,
    layout = wibox.layout.align.horizontal,
    expand = "none"
}
network_toggle_script = [[
  bash -c '
  if [ "$(nmcli radio wifi)" == "enabled" ]; then
    nmcli radio wifi off
  else
    nmcli radio wifi on
  fi
']]
network:buttons(gears.table.join(
    awful.button({ }, 1, function ()
      awful.spawn.with_shell(terminal .. " -e nmtui")
    end),
    awful.button({ }, 3, function ()
      awful.spawn.with_shell(network_toggle_script)
    end)
))

local brightness_icon = wibox.widget.imagebox(beautiful.brightness_icon)
brightness_icon.resize = true
brightness_icon.forced_width = icon_size
brightness_icon.forced_height = icon_size
local brightness_bar = require("noodle.brightness_bar")
brightness_bar.forced_width = progress_bar_width
local brightness = wibox.widget{
    nil,
    {
        brightness_icon,
        pad(1),
        brightness_bar,
        pad(1),
        layout = wibox.layout.fixed.horizontal
    },
    nil,
    expand = "none",
    layout = wibox.layout.align.horizontal
}

brightness:buttons(
  gears.table.join(
    awful.button({ }, 4, function ()
        awful.spawn.with_shell("xbacklight -inc 5")
		awesome.emit_signal("brightness_changed")
    end),
    awful.button({ }, 5, function ()
        awful.spawn.with_shell("xbacklight -dec 5")
		awesome.emit_signal("brightness_changed")
    end)
))

local temperature_icon = wibox.widget.imagebox(beautiful.temperature_icon)
temperature_icon.resize = true
temperature_icon.forced_width = icon_size
temperature_icon.forced_height = icon_size
local temperature_bar = require("noodle.temperature_bar")
temperature_bar.forced_width = progress_bar_width
-- temperature_bar.margins.top = progress_bar_margins
-- temperature_bar.margins.bottom = progress_bar_margins
local temperature = wibox.widget{
  nil,
  {
    temperature_icon,
    pad(1),
    temperature_bar,
    pad(1),
    layout = wibox.layout.fixed.horizontal
  },
  nil,
  expand = "none",
  layout = wibox.layout.align.horizontal
}

local battery_box = require("noodle.battery_bar")
local battery_icon = battery_box:get_all_children()[1]
local battery_bar = battery_box:get_all_children()[3]
battery_icon.forced_width = icon_size
battery_icon.forced_height = icon_size
battery_bar.forced_width = progress_bar_width
local battery = wibox.widget{
  nil,
  battery_box,
  nil,
  expand = "none",
  layout = wibox.layout.align.horizontal
}


local playerctl_toggle_icon = wibox.widget.imagebox(beautiful.playerctl_toggle_icon)
-- local playerctl_toggle_icon = wibox.widget.imagebox(beautiful.playerctl_toggle_icon)
playerctl_toggle_icon.resize = true
playerctl_toggle_icon.forced_width = playerctl_button_size
playerctl_toggle_icon.forced_height = playerctl_button_size
helpers.add_clickable_effect(playerctl_toggle_icon)
playerctl_toggle_icon:buttons(gears.table.join(
                         awful.button({ }, 1, function ()
                             awful.spawn.with_shell("(playerctl play-pause 2>/dev/null && mpc pause) || mpc toggle")
                         end),
                         awful.button({ }, 3, function ()
                             awful.spawn.with_shell(terminal .. " -e ncmpcpp")
                         end)
))

local playerctl_prev_icon = wibox.widget.imagebox(beautiful.playerctl_prev_icon)
playerctl_prev_icon.resize = true
playerctl_prev_icon.forced_width = playerctl_button_size
playerctl_prev_icon.forced_height = playerctl_button_size
helpers.add_clickable_effect(playerctl_prev_icon)
playerctl_prev_icon:buttons(gears.table.join(
                         awful.button({ }, 1, function ()
                             awful.spawn.with_shell("(playerctl previous 2>/dev/null && mpc pause) || mpc prev")
                         end)
))

local playerctl_next_icon = wibox.widget.imagebox(beautiful.playerctl_next_icon)
playerctl_next_icon.resize = true
playerctl_next_icon.forced_width = playerctl_button_size
playerctl_next_icon.forced_height = playerctl_button_size
helpers.add_clickable_effect(playerctl_next_icon)
playerctl_next_icon:buttons(gears.table.join(
                         awful.button({ }, 1, function ()
                             awful.spawn.with_shell("(playerctl next 2>/dev/null && mpc pause) || mpc next")
                         end)
))

local playerctl_buttons = wibox.widget {
  nil,
  {
    playerctl_prev_icon,
    pad(1),
    playerctl_toggle_icon,
    pad(1),
    playerctl_next_icon,
    layout  = wibox.layout.fixed.horizontal
  },
  nil,
  expand = "none",
  layout = wibox.layout.align.horizontal,
}

local time = wibox.widget.textclock("%H %M")
time.align = "center"
time.valign = "center"
time.font = "sans 55"

local date = wibox.widget.textclock("%A, %b %d")
-- local date = wibox.widget.textclock("%A, %B %d")
-- local date = wibox.widget.textclock("%A, %B %d, %Y")
date.align = "center"
date.valign = "center"
date.font = "sans medium 16"

-- local fancy_date = wibox.widget.textclock("%-j days around the sun")
local fancy_date = wibox.widget.textclock("Knowing that today is %A fills you with determination.")
fancy_date.align = "center"
fancy_date.valign = "center"
fancy_date.font = "sans italic 11"

local mpd_song = require("noodle.mpd_song")
local mpd_widget_children = mpd_song:get_all_children()
local mpd_title = mpd_widget_children[1]
local mpd_artist = mpd_widget_children[2]
mpd_title.font = "sans medium 14"
mpd_artist.font = "sans 11"

-- Set forced height in order to limit the widgets to one line.
-- Might need to be adjusted depending on the font.
mpd_title.forced_height = dpi(24)
mpd_artist.forced_height = dpi(16)

local search_icon = wibox.widget.imagebox(beautiful.search_icon)
search_icon.resize = true
search_icon.forced_width = icon_size
search_icon.forced_height = icon_size
local search_text = wibox.widget.textbox("Search")
search_text.font = "sans 14"

local search = wibox.widget{
  search_icon,
  search_text,
  layout = wibox.layout.fixed.horizontal
}
helpers.add_clickable_effect(search)
search:buttons(gears.table.join(
                 awful.button({ }, 1, function ()
                   helpers.reset_cursor()
                   awful.spawn.with_shell("sleep 0.05 && rofi -show drun")
                   sidebar.visible = false
                 end),
                 awful.button({ }, 3, function ()
                   helpers.reset_cursor()
                   awful.spawn.with_shell("sleep 0.05 && rofi -show run")
                   sidebar.visible = false
                 end)
))

local volume_icon = wibox.widget.imagebox(beautiful.volume_icon)
volume_icon.resize = true
volume_icon.forced_width = icon_size
volume_icon.forced_height = icon_size
local volume_bar = require("noodle.volume_bar")
volume_bar.forced_width = progress_bar_width
-- volume_bar.shape = gears.shape.circle
-- volume_bar.margins.top = progress_bar_margins
-- volume_bar.margins.bottom = progress_bar_margins
local volume = wibox.widget{
  nil,
  {
    volume_icon,
    pad(1),
    volume_bar,
    pad(1),
    layout = wibox.layout.fixed.horizontal
  },
  nil,
  expand = "none",
  layout = wibox.layout.align.horizontal
}

volume:buttons(gears.table.join(
                 -- Left click - Mute / Unmute
                 awful.button({ }, 1, function ()
                     awful.spawn.with_shell(volume_script .. " toggle")
                 end),
                 -- Right click - Run or raise pavucontrol
                 awful.button({ }, 3, function ()
                     local matcher = function (c)
                       return awful.rules.match(c, {class = 'Pavucontrol'})
                     end
                     awful.spawn.raise_or_spawn("pavucontrol", { }, matcher)
                 end),
                 -- Scroll - Increase / Decrease volume
                 awful.button({ }, 4, function ()
                     awful.spawn.with_shell(volume_script .. " up")
                 end),
                 awful.button({ }, 5, function ()
                     awful.spawn.with_shell(volume_script .. " down")
                 end)
))

-- Create the sidebar
sidebar = wibox({x = 0, y = 0, visible = false, ontop = true, type = "dock"})
sidebar.bg = beautiful.sidebar_bg or beautiful.wibar_bg or "#111111"
sidebar.fg = beautiful.sidebar_fg or beautiful.wibar_fg or "#FFFFFF"
sidebar.opacity = beautiful.sidebar_opacity or 1
sidebar.height = beautiful.sidebar_height or awful.screen.focused().geometry.height
sidebar.width = beautiful.sidebar_width or dpi(300)
sidebar.y = beautiful.sidebar_y or 0
local radius = beautiful.sidebar_border_radius or 0
if beautiful.sidebar_position == "right" then
  sidebar.x = awful.screen.focused().geometry.width - sidebar.width
  sidebar.shape = helpers.prrect(radius, true, false, false, true)
else
  sidebar.x = beautiful.sidebar_x or 0
  sidebar.shape = helpers.prrect(radius, false, true, true, false)
end
-- sidebar.shape = helpers.rrect(radius)

sidebar:buttons(gears.table.join(
                  -- Middle click - Hide sidebar
                  awful.button({ }, 2, function ()
                      sidebar.visible = false
                  end)
                  -- Right click - Hide sidebar
                  -- awful.button({ }, 3, function ()
                  --     sidebar.visible = false
                  --     -- mymainmenu:show()
                  -- end)
))

-- Hide sidebar when mouse leaves
if beautiful.sidebar_hide_on_mouse_leave then
  local hide_timer = nil
  sidebar:connect_signal("mouse::enter", function ()
	if hide_timer then
      hide_timer:stop()
      hide_timer = nil
	end
  end)
  sidebar:connect_signal("mouse::leave", function ()
	if hide_timer then
      hide_timer:stop()
	end
	if sidebar.visible then
      hide_timer = gears.timer.start_new(0.20, function()
        sidebar.visible = false
		hide_timer = nil
		return false
      end)
    end
  end)
end
-- Activate sidebar by moving the mouse at the edge of the screen
if beautiful.sidebar_hide_on_mouse_leave then
  
  local sidebar_activator = wibox({y = beautiful.sidebar_y or dpi(14), width = 1, visible = true, ontop = false, opacity = 0, below = true})
  sidebar_activator.height = sidebar.height - sidebar_activator.y * 2
  -- sidebar_activator.height = sidebar.height - beautiful.wibar_height
  local sidebar_timer = nil
  sidebar_activator:connect_signal("mouse::enter", function ()
      if not sidebar.visible and not sidebar_timer then
          sidebar_timer = gears.timer.start_new(0.20, function()
            sidebar.visible = true
			sidebar_timer = nil
			return false
          end)
      end
  end)
  sidebar_activator:connect_signal("mouse::leave", function ()
      if sidebar_timer then
          sidebar_timer:stop()
          sidebar_timer = nil
      end
  end)

  if beautiful.sidebar_position == "right" then
    sidebar_activator.x = awful.screen.focused().geometry.width - sidebar_activator.width
  else
    sidebar_activator.x = 0
  end

  sidebar_activator:buttons(
    gears.table.join(
      -- awful.button({ }, 2, function ()
      --     start_screen_show()
      --     -- sidebar.visible = not sidebar.visible
      -- end),
      awful.button({ }, 4, function ()
          awful.tag.viewprev()
      end),
      awful.button({ }, 5, function ()
          awful.tag.viewnext()
      end)
  ))
end

-- Item placement
sidebar:setup {
  { ----------- TOP GROUP -----------
    pad(1),
    pad(1),
    time,
    date,
    pad(1),
    -- fancy_date,
    -- pad(1),
    weather,
    pad(1),
    pad(1),
    layout = wibox.layout.fixed.vertical
  },
  { ----------- MIDDLE GROUP -----------
    playerctl_buttons,
    {
      -- Put some padding at the left and right edge so that
      -- it looks better with extremely long titles/artists
      pad(2),
      mpd_song,
      pad(2),
      layout = wibox.layout.align.horizontal,
    },
    pad(1),
    pad(1),
    volume,
    --cpu,
    brightness,
    battery,
    temperature,
    --ram,
    pad(1),
    --disk,
    network,
    pad(1),
    pad(1),
    layout = wibox.layout.fixed.vertical
  },
  { ----------- BOTTOM GROUP -----------
    { -- Search and exit screen
      nil,
      {
        exit,
        pad(5),
        search,
        pad(2),
        layout = wibox.layout.fixed.horizontal
      },
      nil,
      layout = wibox.layout.align.horizontal,
      expand = "none"
    },
    pad(1),
    layout = wibox.layout.fixed.vertical
  },
  layout = wibox.layout.align.vertical,
  -- expand = "none"
}

-- NOTE:
-- This widget runs a script in the background
-- When awesome restarts, its process will remain alive!
-- Don't worry though! The cleanup script that runs in rc.lua takes care of it :)

local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")

-- Set colors
local title_color =  beautiful.mpd_song_title_color or beautiful.wibar_fg
local artist_color = beautiful.mpd_song_artist_color or beautiful.wibar_fg
local paused_color = beautiful.mpd_song_paused_color or beautiful.normal_fg

-- Set notification icon path
local notification_icon = beautiful.music_icon

local mpd_title = wibox.widget{
  text = "",
  align = "center",
  valign = "center",
  widget = wibox.widget.textbox
}

local mpd_artist = wibox.widget{
  text = "",
  align = "center",
  valign = "center",
  widget = wibox.widget.textbox
}

-- Main widget
local mpd_song = wibox.widget{
  mpd_title,
  mpd_artist,
  layout = wibox.layout.fixed.vertical
}

local artist_fg
local artist_bg

local last_notification_id
local function send_notification(artist, title)
  notification = naughty.notify({
      -- title = "Now playing:",
      -- text = title .. " -- " .. artist,
      title = title,
      text = artist,
      icon = notification_icon,
      -- width = 360,
      -- height = 90,
      -- icon_size = 60,
      timeout = 4,
      replaces_id = last_notification_id
  })
  last_notification_id = notification.id
end

local function update_widget()
  -- awful.spawn.easy_async({"sh", "-c", "mpc"},
  awful.spawn.easy_async({"mpc", "-f", "[[%artist%@@%title%@]]"},
    function(stdout)
      -- naughty.notify({text = stdout})
      -- local artist = stdout:match('(.*)-.*$')
      -- artist = string.gsub(artist, '^%s*(.-)%s*$', '%1')
      -- local title = stdout:match('- (.*)%[')
      -- title = string.gsub(title, '^%s*(.-)%s*$', '%1')
      local artist = stdout:match('(.*)@@')
      if artist == nil or artist == "@" then
        artist = ""
      end
      local title = stdout:match('@@(.*)@')
      if title == nil then
        title = ""
      else
        title = string.gsub(title, '^%s*(.-)%s*$', '%1')
      end
      local status = stdout:match('%[(.*)%]')
      if status == nil then
        status  = "paused"
      else
        status = string.gsub(status, '^%s*(.-)%s*$', '%1')
      end
      if status == "paused" then
        artist_fg = paused_color
        title_fg = paused_color
      else
        artist_fg = artist_color
        title_fg = title_color
        if sidebar.visible == false then
          send_notification(artist, title)
        end
      end

      -- Escape &'s
      title = string.gsub(title, "&", "&amp;")
      artist = string.gsub(artist, "&", "&amp;")

      -- naughty.notify({text = artist .. " - " .. title})
      mpd_title.markup =
        "<span foreground='" .. title_fg .."'>"
        .. title .. "</span>"
      mpd_artist.markup =
        "<span foreground='" .. artist_fg .."'>"
        .. artist .. "</span>"
    end
  )

end

local function update_widget_playerctl(stdout)
  local artist = stdout:match('(.*)@@')
  if artist == nil or artist == "@" then
    artist = ""
  end
  local title = stdout:match('@@(.*)@')
  if title == nil then
    title = ""
  else
    title = string.gsub(title, '^%s*(.-)%s*$', '%1')
  end
  local status = stdout:match('%[(.*)%]')
  if status == nil then
	update_widget()
  else
    awful.spawn.with_shell("mpc pause")
    status = string.gsub(status, '^%s*(.-)%s*$', '%1')
    if status == "paused" then
      artist_fg = paused_color
      title_fg = paused_color
    else
      artist_fg = artist_color
      title_fg = title_color
    end

    -- Escape &'s
    title = string.gsub(title, "&", "&amp;")
    artist = string.gsub(artist, "&", "&amp;")

    mpd_title.markup =
      "<span foreground='" .. title_fg .."'>"
      .. title .. "</span>"
    mpd_artist.markup =
      "<span foreground='" .. artist_fg .."'>"
      .. artist .. "</span>"
    end
end

update_widget()

local mpd_script = [[
  bash -c "
    while [ $(ps x | grep 'mpd' | awk '{print $5}' | grep -cE '^mpd$') -eq 0 ]; do
      sleep 1
    done
	sleep 1
	echo player
    mpc idleloop player
  "]]

local player_script = [[
  bash -c "playerctl metadata --follow --format '{{xesam:artist}}@@{{xesam:title}}@[{{lc(status)}}]' 2>/dev/null"
]]

awful.spawn.with_line_callback(mpd_script, {
                                 stdout = function(line)
                                   update_widget()
                                 end
})

awful.spawn.with_line_callback(player_script, {
                                 stdout = function(line)
                                   update_widget_playerctl(line)
                                 end
})

return mpd_song


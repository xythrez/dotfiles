local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local helpers = require("helpers")
local pad = helpers.pad

-- Configuration
local update_interval = 1            -- in seconds

local network_text = wibox.widget{
  text = "Loading...",
  align  = 'center',
  valign = 'center',
  widget = wibox.widget.textbox
}

local network_icon = wibox.widget.imagebox(beautiful.disconnected_icon)
network_icon.resize = true
network_icon.forced_width = 40
network_icon.forced_height = 40

local network = wibox.widget{
  network_icon,
  pad(1),
  network_text,
  layout = wibox.layout.fixed.horizontal
}

local function update_widget(text)
  if text == "Disconnected\n" or text == "Disabled\n" then
    network_icon.image = beautiful.disconnected_icon
  else
    network_icon.image = beautiful.network_icon
  end
  network_text.markup = text
end

local net_script = [[
  bash -c '
  ADAPTERS=($(find /sys/class/net -regex ".*/[we][ln].*" | awk "{ print \$1 }"))
  WLAN_STATE=$(wifi | awk "{ print \$3 }")

  for ADAPTER in "${ADAPTERS[@]}"; do
  	STATE=$(cat "${ADAPTER}/operstate")
  	if [[ "$ADAPTER" == /sys/class/net/en* ]].."]]"..[[ && ! [ "$STATE" == "down" ]; then
  		echo "Ethernet"
  		exit 0
  	fi
  done

  for ADAPTER in "${ADAPTERS[@]}"; do
  	STATE=$(cat "${ADAPTER}/operstate")
  	if [ "$WLAN_STATE" == "on" ] && [[ $ADAPTER == /sys/class/net/wl* ]].."]]"..[[ && ! [ "$STATE" == "down" ]; then
  		echo "$(nmcli -c no -g NAME connection show -a | head -n1)"
  		exit 0
  	fi
  done

  if [ "$WLAN_STATE" == "off" ]; then
  	echo "Disabled"
  else
  	echo "Disconnected"
  fi
  ']]

awful.widget.watch(net_script, update_interval, function(widget, stdout)
                     update_widget(stdout)
end)

return network

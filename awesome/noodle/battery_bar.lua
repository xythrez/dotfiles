local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local helpers = require("helpers")
local pad = helpers.pad

-- Set colors
local active_color = beautiful.battery_bar_active_color or "#5AA3CC"
local background_color = beautiful.battery_bar_background_color or "#222222"

-- Configuration
local icon_update_interval = 5            -- in seconds
local bar_update_interval = 60            -- in seconds

local battery_icon = wibox.widget.imagebox(beautiful.battery_charging_icon)
battery_icon.resize = true
battery_icon.forced_width = 40
battery_icon.forced_height = 40

local battery_bar = wibox.widget{
  max_value     = 100,
  value         = 50,
    forced_height = dpi(10),
    margins       = {
      top = dpi(8),
      bottom = dpi(8),
    },
    forced_width  = dpi(200),
  shape         = gears.shape.rounded_bar,
  bar_shape     = gears.shape.rounded_bar,
  color         = active_color,
  background_color = background_color,
  border_width  = 0,
  border_color  = beautiful.border_color,
  widget        = wibox.widget.progressbar,
}

local battery = wibox.widget{
  battery_icon,
  pad(1),
  battery_bar,
  pad(1),
  layout = wibox.layout.fixed.horizontal
}

local was_charging = true
local last_bat = 100
local function update_widget_icon(state)
  if state == "1\n" and not was_charging then
    battery_icon.image = beautiful.battery_charging_icon
    was_charging = true
    awesome.emit_signal("charger_plugged")
  elseif state ~= "1\n" and was_charging then
    battery_icon.image = beautiful.battery_icon
    was_charging = false
    awesome.emit_signal("charger_unplugged")
  end
end

local function update_widget(bat)
  local bat_num = tonumber(bat)
  if last_bat ~= bat_num then
    if last_bat < bat_num and bat_num == 100 and was_charging then
      awesome.emit_signal("battery_full")
    elseif last_bat < bat_num and bat_num == 5 and not was_charging then
      awesome.emit_signal("battery_critical")
    elseif last_bat < bat_num and bat_num == 10 and not was_charging then
      awesome.emit_signal("battery_low")
    end
  end
  battery_bar.value = bat_num
  last_bat = bat_num
end

local icon_script = "cat /sys/class/power_supply/AC/online"

local bat_script = [[
  bash -c "
    ACPI_PATH=/sys/class/power_supply
    AC=AC

    BAT_PATH=$(find -L "$ACPI_PATH" -maxdepth 1 -mindepth 1 | grep --invert-match "$ACPI_PATH/$AC")

    ENERGY_FULL=0
    ENERGY_NOW=0

    for BAT in ${BAT_PATH[@]}; do
      BAT_FULL=$(cat "$BAT/energy_full")
      BAT_NOW=$(cat "$BAT/energy_now")
      ENERGY_FULL=$((ENERGY_FULL + BAT_FULL))
      ENERGY_NOW=$((ENERGY_NOW + BAT_NOW))
    done

    echo $((ENERGY_NOW / (ENERGY_FULL / 100)))
  "]]

awful.widget.watch(icon_script, icon_update_interval, function(widget, stdout)
  update_widget_icon(stdout)
end)

awful.widget.watch(bat_script, bar_update_interval, function(widget, stdout)
  update_widget(stdout)
end)

return battery

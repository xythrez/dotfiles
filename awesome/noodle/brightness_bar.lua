local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- Set colors
local active_color = beautiful.brightness_bar_active_color or "#F5C665"
local background_color = beautiful.brightness_bar_background_color or "#514221"

--Configuration
local update_interval = 15

local brightness_bar = wibox.widget{
  max_value     = 100,
  value         = 50,
    forced_height = dpi(10),
    margins       = {
      top = dpi(8),
      bottom = dpi(8),
    },
    forced_width  = dpi(200),
  shape         = gears.shape.rounded_bar,
  bar_shape     = gears.shape.rounded_bar,
  color         = active_color,
  background_color = background_color,
  border_width  = 0,
  border_color  = beautiful.border_color,
  widget        = wibox.widget.progressbar,
}

local function update_widget(bright)
	brightness_bar.value = tonumber(bright)
end

-- Signals
awesome.connect_signal("brightness_changed", function ()
    awful.spawn.easy_async_with_shell("sleep 0.2 && xbacklight -get", function(out)
      update_widget(out)
    end)
end)

awful.widget.watch("xbacklight -get", update_interval, function(widget, stdout)
                    local bright = stdout
                    update_widget(bright)
end)

return brightness_bar

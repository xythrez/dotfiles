#!/usr/bin/env python

import sys
import socket
import imaplib

socket.setdefaulttimeout(3)
obj = imaplib.IMAP4_SSL(sys.argv[1],int(sys.argv[2]))
obj.login(sys.argv[3],sys.argv[4]) # write your email and password
obj.select()
print(len(obj.search(None, 'UnSeen')[1][0].split()))

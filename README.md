Xythrez's Dotfiles
==================================

Xythre's config settings on openbox, also doubles as a working demo for musct configuration

Credits go to [addy-dclxvi](https://github.com/addy-dclxvi) for the original conky theme and tint2 theme.
